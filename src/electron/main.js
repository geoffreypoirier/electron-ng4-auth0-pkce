/**
 * Created by geoffrey on 5/17/17.
 *
 * REFERENCE
 * - https://github.com/auth0-blog/angular2-electron/blob/master/src/electron/main.js
 */
const electron = require('electron');

const app = electron.app;

const BrowserWindow = electron.BrowserWindow;

const globalShortcut = electron.globalShortcut;
const dialog         = electron.dialog;

const path = require('path');
const url  = require('url');

let mainWindow;


function createWindow() {
  mainWindow = new BrowserWindow({show: false, width: 800, height: 600});
  mainWindow.once('ready-to-show', () => {
    mainWindow.show();
  });
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file',
    slashes : true
  }));

  mainWindow.webContents.openDevTools();

  mainWindow.on('closed', () => {
    mainWindow = null;
  })
}


app.on('ready', () => {
  createWindow();
});

app.on('will-quit', () => {
  globalShortcut.unregisterAll();
});

app.on('window-all-closed', () => {
  // kill for OS X
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
