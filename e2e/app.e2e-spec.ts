import { ElectronNg4Auth0PkcePage } from './app.po';

describe('electron-ng4-auth0-pkce App', () => {
  let page: ElectronNg4Auth0PkcePage;

  beforeEach(() => {
    page = new ElectronNg4Auth0PkcePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
